/*****************************************************************************
 * Avalon-MM simple PIO module
 * Author: Pavel Yankovets
 *
 * Functional description:
 * Provides PIO output and input ports
 * Out ports are first in address space
 *****************************************************************************/

`timescale 1 ns / 100 ps
`default_nettype none

module amm_simple_pio
    #(
    parameter int IN_PORTS      = 8, // words
    parameter int OUT_PORTS     = 8, // words
    parameter int SYMBOL_W      = 8,
    parameter int DATA_W        = 32,
    parameter int ADDRESS_W     = ($clog2(IN_PORTS+OUT_PORTS)+$clog2(DATA_W/SYMBOL_W)), // symbols
    parameter int BURSTCOUNT_W  = 1
    )
    (
    /* Interface: clock/reset */
    input  wire                             clk,
    input  wire                             reset_n,

    /* Interface: Avalon-MM slaveA */
    output wire                             slave_waitrequest,
    output wire [DATA_W-1:0]                slave_readdata,
    output wire                             slave_readdatavalid,
    input  wire [BURSTCOUNT_W-1:0]          slave_burstcount,
    input  wire [DATA_W-1:0]                slave_writedata,
    input  wire [ADDRESS_W-1:0]             slave_address,
    input  wire                             slave_write,
    input  wire                             slave_read,
    input  wire [DATA_W/SYMBOL_W-1:0]       slave_byteenable,
    input  wire                             slave_debugaccess,

    /* Interface: PIO */
    output wire [OUT_PORTS-1:0][DATA_W-1:0] pio_out,
    input  wire [IN_PORTS-1:0][DATA_W-1:0]  pio_in
    );

    /**************************************************************************
     * Signals
     **************************************************************************/
    /* Word address */
    wire [$clog2(IN_PORTS+OUT_PORTS)-1:0]    word_address;

    /* Registered signals */
    reg                                     read_reg;
    reg  [DATA_W-1:0]                       readdata_reg;
    reg  [OUT_PORTS-1:0][DATA_W-1:0]        pio_out_reg;
    reg  [IN_PORTS-1:0][DATA_W-1:0]         pio_in_reg;


    /**************************************************************************
     * Word address
     **************************************************************************/
    assign word_address = slave_address >> $clog2(DATA_W/SYMBOL_W);

    /**************************************************************************
     * Store PIO data in registers
     **************************************************************************/
    generate
        genvar i,j;

        for (i=0;i<OUT_PORTS;i=i+1) begin: gen_out_ports
            always @(posedge clk) begin
                if (!reset_n)
                    pio_out_reg[i] <= {DATA_W{1'b0}};
                else if (slave_write)
                    pio_out_reg[i] <= (word_address == i) ? slave_writedata
                                                          : pio_out_reg[i];
            end
        end
        for (j=0;j<IN_PORTS;j=j+1) begin: gen_in_ports
            always @(posedge clk) begin
                if (!reset_n)
                    pio_in_reg[j] <= {DATA_W{1'b0}};
                else
                    pio_in_reg[j] <= pio_in[j];
            end
        end
    endgenerate
    /**************************************************************************
     * Read and readdata
     **************************************************************************/
    always @(posedge clk) begin
        if (!reset_n) begin
            read_reg <= 1'b0;
            readdata_reg <= {DATA_W{1'b0}};
        end else begin
            read_reg <= slave_read;
            if (word_address<OUT_PORTS)
                readdata_reg <= pio_out_reg[word_address];
            else
                readdata_reg <= pio_in_reg[word_address-OUT_PORTS];
        end
    end
    /**************************************************************************
     * Output wires
     **************************************************************************/
    assign slave_readdata = readdata_reg;
    assign slave_readdatavalid = read_reg;
    assign slave_waitrequest = 1'b0;
    assign pio_out = pio_out_reg;


`ifdef COCOTB_SIM
    initial begin
      $dumpfile ("amm_simple_pio.vcd");
      $dumpvars (0, amm_simple_pio);
      #1;
    end
`endif
endmodule

`default_nettype wire
