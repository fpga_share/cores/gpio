cocotb==1.8.1
cocotb-bus==0.2.1
cocotb-test==0.2.4
find-libpython==0.3.1
iniconfig==2.0.0
packaging==23.2
pluggy==1.4.0
pytest==8.0.0
