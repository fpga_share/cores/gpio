#! /usr/bin/env python

import cocotb
from cocotb.triggers import ClockCycles, RisingEdge, Timer
from cocotb.log import SimLog
from cocotb.clock import Clock
from cocotb.runner import get_runner
from cocotb_bus.drivers.avalon import AvalonMaster
from cocotb.binary import BinaryValue

from cocotb_aux.src import binarray

import logging
from pathlib import Path
from pathlib import PurePath
import os
from random import randrange
from math import ceil


class TestBench:
    """Основной класс тестбенча. Включает в себя вспомогательные сущности
    """
    __test__ = False

    TEST_DURATION_CYCLES=40

    dut_in_ports : int = 2
    dut_out_ports : int = 2
    dut_data_width : int = 32

    dut_parameters = {
        "IN_PORTS": f"{dut_in_ports}",
        "OUT_PORTS": f"{dut_out_ports}",
        "DATA_W": f"{dut_data_width}",
        "SYMBOL_W": "8",
        }

    def __init__(self, dut):
        self.dut = dut
        self.log = SimLog("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        self.amm_master = AvalonMaster(dut, "slave", dut.clk)

        self._pio_out_offst = 0x0
        """Смещение группы портов OUT. Всегда нулевое"""

        self.bytes_in_word = ceil(self.dut_data_width / 8)
        """Количество байт в одном слове PIO"""

        self._pio_in_offst = self._pio_out_offst + \
                                        self.dut_in_ports * self.bytes_in_word
        """Смещение группы портов IN, группа начинается сразу после OUT"""

        main_clock = Clock(dut.clk, 10, units="ns")
        cocotb.start_soon(main_clock.start())

    def _reg_out_offst_bytes(self, reg_num: int) -> int:
        """Получить смещение регистра нужного reg_out в байтах"""
        return self._pio_in_offst + reg_num * self.bytes_in_word

    async def wait_for_cycles(self, cycle_count):
        """Подождать указанное количество тактов клока
        """
        for i in range(cycle_count):
            await RisingEdge(self.dut.clk)

    async def _reset(self):
        """Выполнить процедуру сброса
        """

        pin_in = self.dut.pio_in
        pin_in.value = 0
        reset_n = self.dut.reset_n
        reset_n.value = 0
        await self.wait_for_cycles(randrange(10))
        reset_n.value = 1

    def _get_word_from_reg(self, reg_handle, chann_num):
        """Вернуть значение регистра порта из общего массива
           reg_handle: хэндл объекта порта
           chann_num: номер канала, который нужно получить
        """
        lo_idx = chann_num * self.dut_data_width
        size = self.dut_data_width
        return binarray.get_segment_from_vector_size(
                                                vector_handle=reg_handle.value,
                                                lo_idx=lo_idx, size=size)

    def _set_word_in_reg(self, reg_handle, chann_num, val):
        """Установить значение регистра порта из общего массива
           reg_handle: хэндл объекта порта
           chann_num: номер канала, который нужно получить
           val: устанавливаемое значение
        """
        val_bin = BinaryValue(value=val, n_bits=self.dut_data_width,
                              bigEndian=False)
        ret = binarray.set_segment_in_vector(vector_handle=reg_handle.value,
                                       sector_handle=val_bin,
                                       lo_idx=chann_num * self.dut_data_width,
                                       size=self.dut_data_width,
                                       )
        reg_handle.value = ret

    async def _check_write_pio_out(self):
        writed_vals = []
        for i in range(self.dut_in_ports):
            random_range = 1 << self.dut_data_width
            """Какое-то произвольное большое число, ограниченное разрядностью"""
            val = randrange(random_range)
            writed_vals.append(val)
            await self.amm_master.write(i * self.bytes_in_word, val)

        # проверка что считываются те данные что записаны
        for i in range(self.dut_in_ports):
            val = await self.amm_master.read(i * self.bytes_in_word)
            assert val == writed_vals[i]

        # проверка что записанные данные выставились на PIO
        for i in range(self.dut_in_ports):
            pin_out = self.dut.pio_out
            val = self._get_word_from_reg(pin_out, i)
            assert val == writed_vals[i]

    async def _check_write_pio_in(self):
        pin_in = self.dut.pio_in
        reg_addr = self._reg_out_offst_bytes(1)
        await self.wait_for_cycles(1)
        self._set_word_in_reg(pin_in, 1, 5)
        val = await self.amm_master.read(reg_addr)
        assert val == 5


    async def run_tb(self):
        """Основной код testbench
        """
        await self._reset()
        await self.wait_for_cycles(randrange(4))
        await self._check_write_pio_out()
        await self.wait_for_cycles(randrange(4))
        await self._check_write_pio_in()

###############################################################################
# Tests
###############################################################################
@cocotb.test()
async def main_test(dut):
    tb = TestBench(dut)

    tb_thread = cocotb.start_soon(tb.run_tb())
    await tb.wait_for_cycles(tb.TEST_DURATION_CYCLES)
    tb_thread.kill()


###############################################################################
# Test runner
###############################################################################
def test_bench():
    sim = os.getenv("SIM", "icarus")
    runner = get_runner(sim)

    top_module = "amm_simple_pio"
    proj_path = Path(__file__).resolve().parent.parent
    verilog_sources = [proj_path / "hdl" / "amm_simple_pio.sv"]

    runner.build(
        hdl_toplevel=top_module,
        verilog_sources=verilog_sources,
        always=True,
        parameters=TestBench.dut_parameters,
        )

    module = PurePath(PurePath(__file__).name).stem
    runner.test(
        test_module=module,
        hdl_toplevel=top_module,
        hdl_toplevel_lang="verilog",
        waves=True,
        )

if __name__ == '__main__':
    test_bench()
